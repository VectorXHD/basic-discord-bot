// Librairie Discord
const Discord = require("discord.js");
const client = new Discord.Client();

//Charge la configuration
const config = require("./config.json");

//Quand le bot est en ligne
client.on('ready', () => {
	console.log('BOT ON !');
	//Le jeux du bot
  	bot.user.setGame(config.prefix + `help pour l'aide`);
});


client.on('message', msg => {
	//Pour pas que le bot se réponde tout seul
	if(msg.author.bot) return;

	//Si il n'y a pas le préfix
	if(!msg.content.startsWith(config.prefix)) return;

	//Récupert la commande sans le préfix
	let command = msg.content.split(" ")[0];
	command = command.slice(config.prefix.length);

	//Récupert les argument
  	let args = msg.content.split(" ").slice(1);

  	if (command === "ping") {
  		msg.channel.sendMessage("**:ping_pong: | Pong!**");
  	}
});

client.login(config.token);
        